from setuptools import setup, find_packages

setup(
    name="nemo_keycloak_identity_service",
    version="2.2.0",
    python_requires=">=3.8",
    packages=find_packages(),
    author="Atlantis Labs LLC",
    author_email="atlantis@atlantislabs.io",
    url="https://gitlab.com/nemo-community/atlantis-labs/keycloak-identity-service",
    description="The Keycloak Identity service for NEMO allows users to be created and synced up between NEMO and Keycloak. It can create/modify/delete Keycloak user accounts, reset passwords and unlock accounts. NEMO issues commands to the service to affect Keycloak configuration, thereby propagating configuration to other systems.",
    install_requires=["python-keycloak==3.7.0", "Flask==3.0.0", "gunicorn==21.2.0"],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: Flask",
        "Intended Audience :: System Administrators",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
    ],
)
