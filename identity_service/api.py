from http import HTTPStatus
from logging import Logger
from logging.config import dictConfig
from re import match
from typing import Optional, Tuple

from flask import Flask, jsonify, request
from keycloak.exceptions import KeycloakError
from werkzeug.datastructures import MultiDict

from identity_service import configuration
from identity_service.server import Keycloak, NotFound

dictConfig(
    {
        "version": 1,
        "formatters": {"default": {"format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"}},
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)

application = Flask(__name__)

app_logger: Logger = application.logger


def assert_username_is_valid(token):
    if not match("^[A-Za-z0-9_]+$", token):
        raise ValueError(f"Invalid username: {token}")


def assert_email_address_is_valid(token):
    if match("^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,6})$", token) is None:
        raise ValueError(f"Invalid email address: {token}")


@application.route("/", methods=["GET"])
def get():
    skip_response = is_domain_invalid(request.args, json_format=True)
    if skip_response:
        return skip_response
    try:
        username = request.args["username"]
        assert_username_is_valid(username)
        server = Keycloak()
        results = server.search(username)
        response = jsonify(results), HTTPStatus.OK
    except KeyError as e:
        response = "Insufficient or invalid parameters to execute this request", HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except NotFound as e:
        response = str(e), HTTPStatus.NOT_FOUND
        app_logger.exception(e)
    except ValueError as e:
        response = str(e), HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except KeycloakError as e:
        response = "Keycloak Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    except Exception as e:
        response = "Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    return response


@application.route("/", methods=["PUT"])
def put():
    skip_response = is_domain_invalid(request.form)
    if skip_response:
        return skip_response
    try:
        username = request.form["username"]
        assert_username_is_valid(username)
        email = request.form.get("email")
        if email:
            assert_email_address_is_valid(email)
        server = Keycloak()
        result, description = server.create(username, email)
        response = (description, HTTPStatus.OK if result else HTTPStatus.INTERNAL_SERVER_ERROR)
    except KeyError as e:
        response = "Insufficient or invalid parameters to execute this request", HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except ValueError as e:
        response = str(e), HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except NotFound as e:
        response = str(e), HTTPStatus.NOT_FOUND
        app_logger.exception(e)
    except KeycloakError as e:
        response = "Keycloak Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    except Exception as e:
        response = "Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    return response


@application.route("/add/", methods=["PUT"])
def add():
    return "Not applicable", HTTPStatus.METHOD_NOT_ALLOWED


@application.route("/", methods=["DELETE"])
def delete():
    skip_response = is_domain_invalid(request.form)
    if skip_response:
        return skip_response
    try:
        username = request.form["username"]
        assert_username_is_valid(username)
        server = Keycloak()
        result, description = server.delete(username)
        response = (description, HTTPStatus.OK if result else HTTPStatus.INTERNAL_SERVER_ERROR)
    except KeyError as e:
        response = "Insufficient or invalid parameters to execute this request", HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except ValueError as e:
        response = str(e), HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except NotFound as e:
        response = str(e), HTTPStatus.NOT_FOUND
        app_logger.exception(e)
    except KeycloakError as e:
        response = "Keycloak Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    except Exception as e:
        response = "Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    return response


@application.route("/areas/", methods=["GET"])
def areas():
    # This is needed by NEMO but we won't return anything
    return "[]", HTTPStatus.OK


@application.route("/unlock_account/", methods=["POST"])
def unlock_account():
    skip_response = is_domain_invalid(request.form)
    if skip_response:
        return skip_response
    try:
        username = request.form["username"]
        assert_username_is_valid(username)
        server = Keycloak()
        reset, description = server.unlock_account(username)
        response = (description, HTTPStatus.OK if reset else HTTPStatus.INTERNAL_SERVER_ERROR)
    except KeyError as e:
        response = "Insufficient or invalid parameters to execute this request", HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except ValueError as e:
        response = str(e), HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except NotFound as e:
        response = str(e), HTTPStatus.NOT_FOUND
        app_logger.exception(e)
    except KeycloakError as e:
        response = "Keycloak Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    except Exception as e:
        response = "Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
    return response


@application.route("/reset_password/", methods=["POST"])
def reset_password():
    skip_response = is_domain_invalid(request.form)
    if skip_response:
        return skip_response
    try:
        username = request.form["username"]
        assert_username_is_valid(username)
        server = Keycloak()
        reset, description = server.reset_password(username)
        response = (description, HTTPStatus.OK if reset else HTTPStatus.INTERNAL_SERVER_ERROR)
    except KeyError as e:
        response = "Insufficient or invalid parameters to execute this request", HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except ValueError as e:
        response = str(e), HTTPStatus.BAD_REQUEST
        app_logger.exception(e)
    except NotFound as e:
        response = str(e), HTTPStatus.NOT_FOUND
        app_logger.exception(e)
    except KeycloakError as e:
        response = "Keycloak Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
        app_logger.exception(e)
    except Exception as e:
        response = "Exception: " + str(e), HTTPStatus.INTERNAL_SERVER_ERROR
    return response


def is_domain_invalid(request_data: MultiDict, json_format=False) -> Optional[Tuple[str, HTTPStatus]]:
    config_domain = configuration.keycloak_server.get("domain", "")
    request_domain = request_data.get("domain", "")
    if config_domain and request_domain != config_domain:
        return "Invalid domain, skipping" if not json_format else [], HTTPStatus.OK


if __name__ == "__main__":
    application.run()
