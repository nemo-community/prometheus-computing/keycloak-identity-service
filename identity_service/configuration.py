import os

keycloak_server = {
    "url": os.environ.get("KEYCLOAK_URL")
    or f"https://{os.environ['KEYCLOAK_WEB_HOSTNAME']}{(':'+os.environ.get('KEYCLOAK_PORT')) if os.environ.get('KEYCLOAK_PORT') else ''}/",
    "realm": os.environ["KEYCLOAK_REALM"],
    "client_id": os.environ.get("KEYCLOAK_ADMIN_CLIENT_ID", "admin-cli"),
    "client_secret": os.environ["KEYCLOAK_ADMIN_CLIENT_SECRET"],
    "verify": os.environ.get("KEYCLOAK_VERIFY", "True") == "True",
    "disabled_on_delete": os.environ.get("KEYCLOAK_DISABLE_ON_DELETE", "False") == "True",
    "auto_verify_email": os.environ.get("KEYCLOAK_AUTO_VERIFY_EMAIL", "False") == "True",
    "domain": os.environ.get("NEMO_DOMAIN", ""),
    "timeout": 5,
}


def default_password():
    return os.environ["DEFAULT_PASSWORD"]
