# Keycloak Identity Service

Identity service for NEMO and Keycloak, allowing users to be created and synced up between the two

## Configuration
### Identity service
The following environment variables are mandatory:

* `KEYCLOAK_URL` the Keycloak full URL, or `KEYCLOAK_WEB_HOSTNAME` the Keycloak hostname (+ optional port if applicable)
* `KEYCLOAK_REALM` the Keycloak realm to use
* `KEYCLOAK_ADMIN_CLIENT_SECRET` the secret for the client
* `DEFAULT_PASSWORD` the default password for new users and reset password users
  
The following environment variables are optional:
* `KEYCLOAK_PORT` the Keycloak port, defaults to `''` (only used if `KEYCLOAK_WEB_HOSTNAME` is set)
* `KEYCLOAK_ADMIN_CLIENT_ID` the name of the admin-cli client, defaults to `admin-cli`
* `KEYCLOAK_VERIFY` to check SSL certificate, defaults to `'True'`
* `KEYCLOAK_DISABLE_ON_DELETE` to disable user instead of deleting them, defaults to `'False'`
* `KEYCLOAK_AUTO_VERIFY_EMAIL` to have emails automatically marked as verified, defaults to `'False'`
* `NEMO_DOMAIN` the NEMO domain for which to sync users. If this variable is set and doesn't match the one we receive from NEMO, the request is ignored. Defaults to `''`

### Keycloak
Keycloak `admin-cli` client needs to be set to confidential and allow service account.

In Keycloak < 19:
1. In Keycloak, select the proper Realm
2. Select `Clients`
3. Select `admin-cli`
4. Change `access-type` to `confidential`
5. Turn `Service Accounts Enabled` ON
6. Click `Save`
7. Go to `Service Account Roles`
8. In `Client Roles` select `realm-management`
9. Assign `manage-users`, `query-users` and `view-users`
10. Go to the `Credentials` tab
11. There you can find the `Secret` to use here

In Keycloak >= 19:
1. In Keycloak, select the proper Realm
2. Select `Clients`
3. Select `admin-cli`
4. Change `Client authentication` to `on`
5. Check `Service accounts roles`
6. Click `Save`
7. Go to `Service Account Roles`
8. Follow the link to the mappings for `service-account-admin-cli`
9. Go to the tab 'Role mapping'
10. Click `Assign role`
11. Click `Filter by clients`
12. Search for `users` and press enter
13. Check boxes for `manage-users`, `query-users` and `view-users`
14. Click "Assign"
15. Back to `admin-cli` client, go to the `Credentials` tab
16. There you can find the `Secret` to use here


Note: **It's a good idea to set `Brute force detection` in Keycloak realm `Security Defenses` tab**

### NEMO
The identity service needs to be set in NEMO's `settings.py`:

```python
IDENTITY_SERVICE = {
	'available': True,
	'url': 'http://<identity_service_url>:<identity_service_port>/',
	'domains': ['KEYCLOAK'],
}
```

### Domain
As mentioned previously, if the `NEMO_DOMAIN` environ variable is set but doesn't match the user's domain in NEMO, the request is ignored.

This allows to specify more than one domain in `IDENTITY_SERVICE`, for example `'domains': ['INTERNAL', KEYCLOAK']` but only users assigned the `KEYCLOAK` domain will be managed/synced.<br>
It is useful in cases where multiple authentication options are available.
